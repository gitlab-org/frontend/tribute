# 1.0.0 (2020-12-03)


### Bug Fixes

* Integrate with [@semantic-release](https://gitlab.com/semantic-release) ([67c8ce6](https://gitlab.com/gitlab-org/frontend/tribute/commit/67c8ce6cc746562e62e98408577c8becd260b952))
* **getTriggerInfo:** handle leading space ([#49](https://gitlab.com/gitlab-org/frontend/tribute/issues/49)) ([41c4c65](https://gitlab.com/gitlab-org/frontend/tribute/commit/41c4c655eb36b236da35a03c17e9a51354241abe))
* **tribute:** only reposition menu when enough space is available ([#177](https://gitlab.com/gitlab-org/frontend/tribute/issues/177)) ([017a394](https://gitlab.com/gitlab-org/frontend/tribute/commit/017a3948abf2a8fc40b4f41f48b6ad02790e7bb8)), closes [#175](https://gitlab.com/gitlab-org/frontend/tribute/issues/175)
* **tribute:** use mouseup events instead of mousedown events ([#176](https://gitlab.com/gitlab-org/frontend/tribute/issues/176)) ([31a7f67](https://gitlab.com/gitlab-org/frontend/tribute/commit/31a7f673008352b73282ee27c76ebb741715206f)), closes [#172](https://gitlab.com/gitlab-org/frontend/tribute/issues/172)


### Reverts

* Revert "Fix append and main ref in package.json. Bump version." ([fdcacd2](https://gitlab.com/gitlab-org/frontend/tribute/commit/fdcacd2c421212b51b94fc5d31ff64a102be3ccd))
